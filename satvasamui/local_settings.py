import os
from pathlib import Path

SECRET_KEY = 'django-insecure-)=3q(68+yw$@j@s%cxu=qmqg50+=msag0&h*_p90a8mj_b2fb4'

BASE_DIR = Path(__file__).resolve().parent.parent

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

STATIC_DIR = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = [STATIC_DIR]

RECIPIENTS_EMAIL = ['santexc85@gmail.com']  # замените на свою почту
DEFAULT_FROM_EMAIL = 'admin@mysite.com'  # замените на свою почту
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
