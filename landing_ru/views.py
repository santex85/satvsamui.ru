
from django.core.mail import send_mail

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views import View

from landing_ru.forms import CustomerForm


class IndexView(View):

    @staticmethod
    def get(request):
        return render(request, 'landing_ru/index.html')

    @staticmethod
    def post(request):
        form = CustomerForm(request.POST)

        if form.is_valid():
            client = form.save()
            message_for_client = f"""Здравствуйте {client.name if client.name else 'Дорогой Гость'}! Вы получили день в подарок от Satva Samui. 
В ближайшее время с вами свяжется наш менеджер.
С уважением команда Satva Samui.
            """
            message_for_stuff = f"""Заявка с сайта satvasamui.ru. 
Имя: {client.name if client.name else 'Нет имени'}
Email: {client.email if client.email else 'Нет email'}
Phone: {client.phone if client.phone else 'нет номера телефона'}"""

            if client.email:
                try:
                    send_mail('Это автоматическое сообщение. Не отвечайте на него.', message_for_client,
                              'robot@satvasamui.ru', [client.email])
                    send_mail('Срочно! Заявка.', message_for_stuff, 'robot@satvasamui.ru',
                              ['santex85@gmail.com', 'olga@satvasamui.com'])
                    return HttpResponse(status=200)

                except Exception as ex:
                    print(ex)
                    return HttpResponse(status=500)
            else:

                try:
                    send_mail('Срочно! Заявка.', message_for_stuff, 'robot@satvasamui.ru',
                              ['santex85@gmail.com', 'olga@satvasamui.com'])
                    return HttpResponse(status=200)
                except Exception as ex:
                    print(ex)
                    return HttpResponse(status=500)

        else:
            return JsonResponse(form.errors)
