from django.forms import ModelForm

from landing_ru.models import Customer


class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ('name', 'email', 'phone')
