from django.apps import AppConfig


class LandingRuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'landing_ru'
