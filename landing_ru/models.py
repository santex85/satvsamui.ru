from django.db import models


class Customer(models.Model):
    name = models.CharField('Имя', max_length=200, null=True, blank=True)
    email = models.EmailField('Email', max_length=250, null=True, blank=True)
    phone = models.CharField('Номер телефона', max_length=15, null=True, blank=True)
    create_date = models.DateTimeField('Create date', auto_now_add=True)

    def __str__(self):
        return self.email
