// change date
let date = new Date()
let test = date.setDate(date.getDate() + 3)
let options = {
    month: 'long',
    day: 'numeric',
};
res = date.toLocaleString("ru", options)
document.getElementById('change-date-sm').innerHTML = res
document.getElementById('change-date-xl').innerHTML = res


// click button
document.getElementById('sendData').onclick = function () {
    let data_form = document.forms.modalForm;
    document.getElementById('spinner-border').style.display = 'block'
    document.getElementById('bodyModal').style.display = 'none'
    sendFormData(data_form)
}

function sendFormData(data, url = '') {
    // Send data to server
    let form_data = new FormData(data)
    let request = new XMLHttpRequest();
    request.open('POST', url)
    request.onreadystatechange = function () {

        if (request.readyState === request.DONE && request.status === 500) {
            document.getElementById('spinner-border').style.display = 'none'
            document.getElementById('bodyModal').style.display = 'block'
            document.getElementById('bodyModal').innerHTML = 'Ошибка отправки сообщения'
        }

        if (request.readyState === request.DONE && request.status === 200) {
            console.log(request.responseText)
            document.getElementById('spinner-border').style.display = 'none'
            document.getElementById('bodyModal').style.display = 'block'
            document.getElementById('bodyModal').innerHTML = 'Спасибо за обращение. Вы получили день в подарок! В ближайшее время с вами свяжется наш менеджер.'
        }
    };

    request.send(form_data);


}